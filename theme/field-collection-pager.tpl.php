<ul class="pager-links">
	<?php if ( isset($next_link) ) : ?><li><a href="<?php print $next_link; ?>" rel="next"><?php print t('Next') ?></a><?php endif; ?>
	<?php if ( isset($prev_link) ) : ?><li><a href="<?php print $prev_link; ?>" rel="next"><?php print t('Prev') ?></a><?php endif; ?>
</ul>